const Discord = require('discord.js');
const client = new Discord.Client();

client.on('ready', () => {
    console.log(`Bot started and is logged in as ${client.user.tag}!`);
});

client.on('ready', () => {
    let guilds = client.guilds.cache.array();
    for (let guild of guilds) {
        if (guild.roles.cache.find(x => x.name === "someone")) {
            console.log("The someone role exists");
            return;
        }
        guild.roles.create({
            data: {
                name: "someone"
            }
        }).then(() => console.log("Role someone created")).catch(console.error);
    }
});

client.on('ready', () => {
    let guilds = client.guilds.cache.array();
    for (let guild of guilds) {
        if (guild.roles.cache.find(x => x.name === "SomeoneAdmin")) {
            console.log("The SomeoneAdmin role exists");
            return;
        }
        guild.roles.create({
            data: {
                name: "SomeoneAdmin"
            }
        }).then(() => console.log("Role SomeoneAdmin created")).catch(console.error);
    }
});

client.on('message', async message => {
    let someoneRole = message.guild.roles.cache.find(role => role.name === "someone")
    if (message.author.bot
        || !message.member.roles.cache.find(role => role.name === "SomeoneAdmin")
        || !message.mentions.roles.find(role => role.id === someoneRole.id)
    ) {
        return;
    }

    /** @type Collection<Snowflake, GuildMember> */
    let members = await message.guild.members.fetch();

    // send to
    let chosenUser = members.random();

    // selects random non bot user
    do {
        chosenUser = members.random();
    } while (chosenUser.user.bot);

    // input message
    $storedMessage = message.content;
    message.delete()

    // output message
    $editedStoredMessage = $storedMessage.replace(someoneRole.id, "").replace(`<@&>`, "")

    // gets details of the sender of the message
    $storedAuthor = message.author.tag;
    $storedAuthorIMG = message.author.avatarURL();

    // gets time and date for the end of the message

    $storedDateTime = message.createdAt;

    message.channel.send(`${chosenUser}`);

    const embed = {
        "description": $editedStoredMessage,
        "color": 46259,
        "timestamp": $storedDateTime,
        "author": {
            "name": $storedAuthor,
            "icon_url": $storedAuthorIMG
        }
    };
    message.channel.send({embed});

});

client.login(process.env.TOKEN);